package cz.yetanotherview.listapplication.objects;

public class Employer {

    private int mImageDrawable;
    private String mImageUrl;
    private String mName;
    private String mDepartment;

    public Employer(final String name, final String department, final String imageUrl) {
        mImageUrl = imageUrl;
        mName = name;
        mDepartment = department;
    }

    public Employer(final String name, final String department, final int imageDrawable) {
        mImageDrawable = imageDrawable;
        mName = name;
        mDepartment = department;
    }

    public int getImageDrawable() {
        return mImageDrawable;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getName() {
        return mName;
    }

    public String getDepartment() {
        return mDepartment;
    }

}
