package cz.yetanotherview.listapplication;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import cz.yetanotherview.listapplication.adapters.RecyclerViewAdapter;
import cz.yetanotherview.listapplication.objects.Employer;

public class ListActivity extends AppCompatActivity {

    @IntDef({NumberOfColumns.ONE, NumberOfColumns.TWO})
    @Retention(RetentionPolicy.SOURCE)
    public @interface NumberOfColumns {
        int ONE = 1;
        int TWO = 2;
    }

    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(100,
                StaggeredGridLayoutManager.VERTICAL);

        setSpanCountInLayoutManager(getResources().getConfiguration().orientation);

        final RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mStaggeredGridLayoutManager);

        final Employer prvaOsoba = new Employer("Prva Osoba", "Developer", R.drawable.prva);
        final Employer druhaOsoba = new Employer("Druha Osoba", "Support", R.drawable.druha);
        final Employer tretiaOsoba = new Employer("Tretia Osoba", "Tester", R.drawable.tretia);
        final Employer stvrtaOsoba = new Employer("Stvrta Osoba", "Managment", R.drawable.stvrta);

        final Employer testOsoba = new Employer("Test Osoba", "Test Department",
                "https://igx.4sqi.net/img/user/original/14709722-PEWUTVUAEU11IZPU.jpg");

        final List<Employer> employers = new ArrayList<>();
        employers.add(testOsoba);
        employers.add(druhaOsoba);
        employers.add(tretiaOsoba);
        employers.add(stvrtaOsoba);
        employers.add(prvaOsoba);
        employers.add(testOsoba);
        employers.add(tretiaOsoba);
        employers.add(stvrtaOsoba);
        employers.add(prvaOsoba);
        employers.add(druhaOsoba);
        employers.add(tretiaOsoba);
        employers.add(stvrtaOsoba);

        RecyclerView.Adapter mAdapter = new RecyclerViewAdapter(employers);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setSpanCountInLayoutManager(newConfig.orientation);
    }

    private void setSpanCountInLayoutManager(final int currentOrientation) {
        if (mStaggeredGridLayoutManager == null) {
            return;
        }

        switch (currentOrientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                mStaggeredGridLayoutManager.setSpanCount(NumberOfColumns.ONE);
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                mStaggeredGridLayoutManager.setSpanCount(NumberOfColumns.TWO);
                break;
            default:
                finish();
                break;
        }
    }

}
