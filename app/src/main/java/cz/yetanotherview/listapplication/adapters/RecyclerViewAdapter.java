package cz.yetanotherview.listapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import cz.yetanotherview.listapplication.R;
import cz.yetanotherview.listapplication.objects.Employer;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private final List<Employer> mEmployerList;

    public RecyclerViewAdapter(final List<Employer> employerList) {
        mEmployerList = employerList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mEmployerImage;
        private TextView mEmployerName;
        private TextView mEmployerDepartment;

        ViewHolder(View recyclerItemView) {
            super(recyclerItemView);

            mEmployerImage = (ImageView) recyclerItemView.findViewById(R.id.employerImage);
            mEmployerName = (TextView) recyclerItemView.findViewById(R.id.employerName);
            mEmployerDepartment = (TextView) recyclerItemView.findViewById(R.id.employerDepartment);
        }
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.recycler_item_view, parent, false);

        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Employer employerOnPosition = mEmployerList.get(position);

        if (!TextUtils.isEmpty(employerOnPosition.getImageUrl())) {
            Glide.with(holder.mEmployerImage.getContext())
                    .load(employerOnPosition.getImageUrl())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.mEmployerImage);
        } else if (employerOnPosition.getImageDrawable() != 0) {
            Glide.with(holder.mEmployerImage.getContext())
                    .load(employerOnPosition.getImageDrawable())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.mEmployerImage);
        }

        holder.mEmployerName.setText(employerOnPosition.getName());
        holder.mEmployerDepartment.setText(employerOnPosition.getDepartment());
    }

    @Override
    public int getItemCount() {
        return mEmployerList.size();
    }
}